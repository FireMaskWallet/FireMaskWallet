# FireMask

The web3 crypto wallet and browser extension!

If you want to report a security issue, please [contact us on Twitter](https://twitter.com/firemaskwallet).

You can find the code for our various products and components in this org.

For other information, visit [our main website](https://thefiremask.com).

